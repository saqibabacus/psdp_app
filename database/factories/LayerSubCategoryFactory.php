<?php

namespace Database\Factories;

use App\Models\LayerSubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class LayerSubCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LayerSubCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
