<?php

namespace Database\Factories;

use App\Models\LayerCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class LayerCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LayerCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
