<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto">
            <!-- <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                hi text will show there
            </div> -->
            <div class="grid grid-cols-2 gap-2">
                <div>
                    Text is there 1
                </div>
                <div class="col-span-1">
                    Text is there 2
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
