<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigation-label">Layers Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="{{$type == 'manage' ? 'active' : '' }}">
                <a href="{{route('dashboard')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                    <span class="pcoded-mtext">Manage Layers</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{$type == 'add' ? 'active' : '' }}">
                <a href="{{route('add-layers')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                    <span class="pcoded-mtext">Add Layers</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{$type == 'manage-categories' ? 'active' : '' }}">
                <a href="{{route('manage-categories')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-dashboard"></i></span>
                    <span class="pcoded-mtext">Manage Categories</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </div>
</nav>
