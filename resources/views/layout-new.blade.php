<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="GIS Application">
    <meta name="keywords" content="gis">
    <title>GIS Application</title>
    <!-- Favicons-->
    <link rel="icon" href="{{asset('lib/images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{asset('lib/images/favicon/apple-touch-icon-152x152.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="{{asset('lib/images/favicon/mstile-144x144.png')}}">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{asset('lib/css//materialize.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('lib/css//style.css')}}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{asset('lib/css/custom/custom.css')}}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{asset('lib/vendors/prism/prism.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('lib/vendors/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('lib/vendors/flag-icon/css/flag-icon.min.css')}}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('jquery-ui-1.12.1/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('libraries/leaflet.css')}}"/>
    <style>
        input[type="checkbox"]:not(:checked), input[type="checkbox"]:checked{
            position: relative;
            opacity: 1;
            pointer-events: all;
        }
        input[type="radio"]:not(:checked), input[type="radio"]:checked {
            position: relative;
            opacity: 1;
            pointer-events: all;
        }
        .stylePopup .leaflet-popup-content-wrapper,
        .stylePopup .leaflet-popup-tip {
            background-color: #f4913b;
            padding: 8px;
        }
        .leaflet-tooltip {
            position: absolute;
            padding: 6px;
            font-weight: bold !important;
             background-color: transparent !important;
             border: none !important;
             border-radius: 0px !important;
            color: #FFFFFF !important;
            white-space: normal !important;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            pointer-events: none;
             /*box-shadow: 0 1px 3px rgb(0 0 0 / 40%);*/
             box-shadow: 0 0 0 rgb(0 0 0 ) !important;
        }
        .input-field select {
            position: relative !important;
        }
        .card-panel {
            margin: 0 !important;
        }
        .ui-accordion .ui-accordion-content {
            padding: 0em 0em !important;
            border-top: 0;
            overflow: auto;
            height: 100% !important;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
            border: 1px solid #c5c5c5;
            background: #42a5f5 !important;
            font-weight: normal;
            color: #FFFFFF;
        }
    </style>
</head>
<body>
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-light-blue-cyan">
            <div class="nav-wrapper">
                <ul class="left">
                    <li>
                        <h1 class="logo-wrapper">
                            <a href="index.html" class="brand-logo darken-1">
                                <img src="{{asset('lib/images/logo/materialize-logo.png')}}" alt="materialize logo">
                                <span class="logo-text hide-on-med-and-down">GIS Application</span>
                            </a>
                        </h1>
                    </li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                            <i class="material-icons">settings_overscan</i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <img src="{{asset('lib/images/avatar/avatar-7.png')}}" alt="avatar">
                    <i></i>
                  </span>
                        </a>
                    </li>
                </ul>
                <ul id="profile-dropdown" class="dropdown-content">
                    {{--<li>--}}
                        {{--<a href="#" class="grey-text text-darken-1">--}}
                            {{--<i class="material-icons">face</i> Profile</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#" class="grey-text text-darken-1">--}}
                            {{--<i class="material-icons">settings</i> Settings</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#" class="grey-text text-darken-1">--}}
                            {{--<i class="material-icons">live_help</i> Help</a>--}}
                    {{--</li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li>--}}
                        {{--<a href="#" class="grey-text text-darken-1">--}}
                            {{--<i class="material-icons">lock_outline</i> Lock</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="{{route('login')}}" class="grey-text text-darken-1">
                            <i class="material-icons">keyboard_tab</i> Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper" style="height: 100%;">
        @yield('content')
    </div>
</div>
<!-- ================================================
Scripts
================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="{{asset('lib/vendors/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('jquery-ui-1.12.1/jquery-ui.js')}}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{asset('lib/js/materialize.min.js')}}"></script>
<!--prism-->
<script type="text/javascript" src="{{asset('lib/vendors/prism/prism.js')}}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{asset('lib/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{asset('lib/js/plugins.js')}}"></script>
<script src="{{asset('libraries/leaflet.js')}}"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/@turf/turf@5/turf.min.js"></script>--}}
<script src="https://unpkg.com/@turf/turf@6.3.0/turf.min.js"></script>
@stack('scripts')
</body>
</html>
