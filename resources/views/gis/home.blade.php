@extends('layout')

@section('content')
    <div class="row" style="height: 10%;">
        <div class="col-12">
            <div class="card" style="height: 100%">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
    <div class="row" style="height: 90%;">
        <div class="col-3">
            <div class="card" style="height: 100%">
                <div class="card-body">
                    <div class="form-group">
                        <label for="districtSelect">Select District</label>
                        <select class="form-control" id="districtSelect">
                            <option disabled>Select District</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="card" style="height: 100%">
                <div class="card-body" id="map">

                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script  src="{{asset('functions/main.js')}}"></script>
@endpush
