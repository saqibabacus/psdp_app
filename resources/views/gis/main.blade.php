@extends('layout-new')

@section('content')
    <div class="row" style="height: 100%;">
        <div class="col l2 card-panel" style="height: 100%;padding: 0px;">
            <div class="row">
                <div class="col l12" style="padding: 0px;">
                    <div id="accordion">
                        <h3>Filter Panel</h3>
                        <div class="row">
                            <div class="col l12">
                                <div class="input-field">
                                    <select
                                        style="opacity: 1; width: 100%;pointer-events: all;height: 50px;"
                                        class="select2 browser-default" name="gis_districts" id="gis_districts">
                                        <option value="%">Select District</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col l12">
                                <div class="input-field">
                                    <select
                                        style="opacity: 1; width: 100%;pointer-events: all;height: 50px;"
                                        class="select2 browser-default" name="gis_tehsils" id="gis_tehsils">
                                        <option value="%">Select Tehsil</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h3>Buffer/Analysis</h3>
                        <div class="row">
                            <div class="col l12" style="text-align: center;">
                                <button class="btn-floating waves-effect waves-light gradient-45deg-indigo-light-blue" id="gis_droppedpin" ><i class="material-icons">place</i></button>
                            </div>
                            <div class="col l12">
                                <div class="input-field">
                                    <select
                                        style="opacity: 1; width: 100%;pointer-events: all;height: 50px;"
                                        class="select2 browser-default" name="gis_buffer" id="gis_buffer">
                                        <option value="0.5">0.5 Km</option>
                                        <option value="1" selected>1 Km</option>
                                        <option value="2">2 Km</option>
                                        <option value="3">3 Km</option>
                                        <option value="4">4 Km</option>
                                        <option value="5">5 Km</option>
                                        <option value="50">50 Km</option>
                                        <option value="100">100 Km</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col l12" style="text-align: center;margin-top: 20px;">
                                <button class="btn-floating waves-effect waves-light gradient-45deg-indigo-light-blue" id="gis_btn_buffer" disabled><i class="material-icons">create</i></button>
                                <button class="btn-floating waves-effect waves-light gradient-45deg-red-pink" id="gis_btn_clrbuffer" disabled ><i class="material-icons">clear</i></button>
                            </div>
                        </div>
                        <h3>Layer Tools</h3>
                        <div>
                            <div class="row" id="appendOtherLayers">
                                <div class="col l12">
                                    <div class="mt-4">
                                        <input type="checkbox" id="checkbox_slide_districts" checked>Districts layer
                                    </div>
                                    <input id="slide_districts" type="range" min="0" max="1" step="0.1" value="0.8">
                                </div>
                                <div class="col l12">
                                    <div class="mt-4">
                                        <input type="checkbox" id="checkbox_slide_tehsils">Tehsils layer
                                    </div>
                                    <input id="slide_tehsils" type="range" min="0" max="1" step="0.1" value="0.8">
                                </div>
                                <div class="row" id="appendOtherLayersDynamically">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l8" style="height: 100%;">
            <section id="content" style="height: 100%;">
                <div id="map" style="height: 100%;">

                </div>
            </section>
        </div>
        <div class="col l2" style="height: 100%;overflow: auto;" class="card-panel">
            <div class="header-navbar" style="width: 100%;position: absolute;background: white;">
                <h5>Buffer Summary Panel</h5>
            </div>
            <section id="summaryInfo" style="height: 100%;margin-top:15%;">
                <div style="overflow: auto;" id="bufferTable">

                </div>
            </section>
        </div>

        <div class="row">
            <div class="col l4 mt-1">
                <div class="card-panel">
                    <div class="card-header">
                        <h5>Overall Asset Summary</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Asset Name</th>
                                    <th>Level</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="tableSummary">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col l8 mt-1">
                <div class="card-panel">
                    <div class="card-header">
                        <h5>Buffer Results</h5>
                    </div>
                    <div class="card-body">
                        <div id="bufferResultsDetails" style="max-height: 400px;overflow: auto;">

                        </div>
                    </div>
                </div>
            </div>
        </div>

{{--        <div class="row">--}}
{{--            <div class="col l12 card-panel">--}}
{{--                <h4>Buffer Results</h4>--}}
{{--                <div id="bufferResultsDetails" style="height: 200px;overflow: auto;">--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="row" id="draggable" style="height:350px;width:250px;position: absolute;z-index: 9000;display: none;top:20%;left:40%;">
            <button style="position:absolute;top: -10px;right: -10px;" class="btn-sm btn-floating mb-1 waves-effect waves-light " id="gis_btn_close_buffer" ><i class="material-icons">clear</i></button>
            <div class="col l12 card-panel" style="width: 100%;height: 100%;overflow: auto;">
                <div class="row">
                    <div class="col l12">
                        <h5>Buffer Results</h5>
                    </div>
{{--                    <div class="col l12" id="bufferTable">--}}

{{--                    </div>--}}
                </div>
                {{--<table class="bordered" id="bufferTable">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>ID</th>--}}
                        {{--<th>Name</th>--}}
                        {{--<th>Latitude</th>--}}
                        {{--<th>Longitude</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}

                    {{--</tbody>--}}
                {{--</table>--}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function displaySUmmary(province, type) {
            $.ajax({
                url: "{{ url('api/get_summary_single')}}",
                method: 'POST',
                data: {'_prv': province, '_typ': type},
                success: (data) => {
                    $("#tableSummary").empty();
                    for(var k=0; k<data.length; k++) {
                        var rowAppend = '<tr>' +
                            '<td>'+data[k].layer_name+'</td>' +
                            '<td>'+data[k].name+'</td>' +
                            '<td>'+data[k].total+'</td>' +
                            '</tr>';
                        $("#tableSummary").append(rowAppend);
                    }
                    console.log(data);
                },
                error: (error) => {
                    console.log(error);
                }
            });
        }
    </script>
    <script  src="{{asset('functions/main.js')}}"></script>
    <script>
        $(document).ready(function () {
            $( "#draggable" ).draggable();
            $( "#accordion" ).accordion();
            $("#gis_btn_close_buffer").click(function () {
                // $("#draggable").css("display", "none");
            });
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();

            $.ajax({
                url: "{{ url('api/get_other_layers')}}",
                {{--url: "{{ url('api/get_other_layers')}}",--}}
                method: 'GET',
                success: (data) => {
                    console.log(data);
                    // var sampleData = [
                    //     {
                    //         category_name: "Education",
                    //         sub_category: [
                    //             {
                    //                 sub_category_name: "Primary School",
                    //                 name: "Primary Name",
                    //                 table_name: "primary_table_name"
                    //             },
                    //             {
                    //                 sub_category_name: "Secondary School",
                    //                 name: "Secondary Layer Name",
                    //                 table_name: "secondary_table_name"
                    //             },
                    //         ]
                    //     },
                    //     {
                    //         category_name: "Other Category",
                    //         sub_category: [
                    //             {
                    //                 sub_category_name: "Other School",
                    //                 name: "Other Name",
                    //                 table_name: "other_table_name"
                    //             }
                    //         ]
                    //     }
                    // ];
                    var htmlAppend = '<div class="col l12">\n' +
                        '                                        <p style="font-size: 20px;">Category</p>\n' +
                        '                                        <p style="font-size: 16px;margin-left:15px;">Sub Category</p>\n' +
                        '                                        <div style="margin-left: 20px;">\n' +
                        '                                            <div class="mt-4">\n' +
                        '                                                <input type="checkbox" id="checkbox_slide_tehsils">Tehsils layer\n' +
                        '                                            </div>\n' +
                        '                                            <input id="slide_tehsils" type="range" min="0" max="1" step="0.1" value="0.8">\n' +
                        '                                        </div>\n' +
                        '                                    </div>';
                    // console.log(sampleData);
                    // for(var i=0; i<data.length; i++) {
                    //     var slideId = "slide_" + data[i].table_name;
                    //     var checkBoxId = "" + data[i].table_name;
                    //     var ht = '<div class="col l12"><div class="mt-4"><input onchange="otherLayerCheckBox(this.id)" type="checkbox" id="'+checkBoxId+'">'+data[i].name+'</div><input onchange="otherLayerSlider(this.id)" id="'+slideId+'" data-id="'+checkBoxId+'" type="range" min="0" max="1" step="0.1" value="0.8"></div>';
                    //     $("#appendOtherLayersDynamically").append(htmlAppend);
                    // }
                    for(var i=0; i<data.length; i++) {
                        var ht = "";
                        ht += "<div class='col l12'><p style='font-size: 20px;'>"+data[i].category_name+"</p>";
                        var htt = "";
                        for(var j=0; j<data[i]['sub_category_name'].length; j++) {
                            var checkBoxId = "" + data[i]['sub_category_name'][j].table_name;
                            var slideId = "slide_" + data[i]['sub_category_name'][j].table_name;
                            htt += '<p style="font-size: 16px;margin-left:15px;">'+data[i]['sub_category_name'][j].sub_category_name+'</p>';
                            htt += '<div style="margin-left: 20px;"><div class="mt-4">' +
                                '<input onchange="otherLayerCheckBox(this.id)" type="checkbox" id="'+checkBoxId+'">'+data[i]['sub_category_name'][j].name+'</div>' +
                                '<input onchange="otherLayerSlider(this.id)" id="'+slideId+'" data-id="'+checkBoxId+'" type="range" min="0" max="1" step="0.1" value="0.8">' +
                                '</div>'
                        }
                        $("#appendOtherLayersDynamically").append(ht + htt + "</div>");
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            });

            displaySUmmary('BALOCHISTAN', 'PROVINCE');
            if({{count($d)}} > 0) {
                var lat = {{$d["lat"] ?? 'qq'}};
                var lng = {{$d["lon"] ?? 'qq'}};
                showPoint(lat,lng);
            }
        });

        function showPoint(lat, lng) {
            var icn = L.icon({
                iconUrl: '../images/marker-url.png',
                iconSize: [24, 38],
            });
            var marker = L.marker([lat, lng],{
                icon: icn
            }).addTo(map);
        }

        function otherLayerCheckBox(val) {
            // alert(val);
            // console.log(val);
            // console.log(anonymousLayerGroup.getLayers());
            anonymousLayerGroup.eachLayer(function(layer){
                if(layer.options.id == val) {
                    if($("#"+val).is(':checked')) {
                        map.addLayer(layer);
                    } else {
                        map.removeLayer(layer);
                    }
                }
            });
        }

        function otherLayerSlider(id) {
            anonymousLayerGroup.eachLayer(function(layer){
                if(layer.options.id == $("#"+id).attr('data-id')) {
                    layer.setStyle({ fillOpacity: $("#"+id).val() });
                }
            });
        }
    </script>
@endpush
