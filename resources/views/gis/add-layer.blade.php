@extends('layouts.layout')
@section('content')

    @include('layouts.nav')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            @include('layouts.sidebar', ['type' => 'add'])
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Browser Layers</h5>
                                            </div>
                                            <div class="card-block">
                                                {{--<input type="file" id="shapeFileBorwser" name="shapeFileBorwser" />--}}
                                                <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)" >
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="file" name="shapeFileBorwser" placeholder="Choose File" id="shapeFileBorwser">
                                                                <span class="text-danger">{{ $errors->first('file') }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="styleSelector"></div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('libraries/leaflet.js')}}"></script>
    <script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.3.1/leaflet-omnivore.min.js'></script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            {{--alert("{{asset('storage/csvfiles/data.csv')}}");--}}
            var layer = omnivore.csv("{{asset('storage/csvfiles/empty.csv')}}");
            console.log(layer);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#laravel-ajax-file-upload').submit(function(e) {
                console.log($('meta[name="csrf-token"]'))
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url: "{{ url('store-file')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        this.reset();
                        alert('File has been uploaded successfully');
                        console.log(data);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });
    </script>
@endpush
