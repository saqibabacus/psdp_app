@extends('layouts.layout')
@section('content')

    @include('layouts.nav')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            @include('layouts.sidebar', ['type' => 'manage'])
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="card h-100">
                                            <div class="card-header">
                                                <h5>Admin Layers</h5>
                                            </div>
                                            <div class="card-block">
                                                @foreach($layers as $layer)
                                                    <div id="adminLayers">
                                                        <input
                                                            type="checkbox"
                                                            value="{{$layer->id}}"
                                                            class="adminCheckbox"
                                                            id="{{$layer->table_name}}"
                                                            name="layersCheckbox" {{$layer->display == true ? 'checked' : ''}} /> {{ $layer->name }}
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="card h-100">
                                            <div class="card-header">
                                                <h5>Other Layers</h5>
                                            </div>
                                            <div class="card-block">
                                                <table class="table table-hover table-light">
                                                    <thead>
                                                    <tr>
                                                        <th style="font-weight: 700;">Serial</th>
                                                        <th style="font-weight: 700;">Layer Name</th>
                                                        <th style="font-weight: 700;">Category</th>
                                                        <th style="font-weight: 700;">Subcategory</th>
                                                        <th style="font-weight: 700;">Enable/Disable</th>
                                                        <th style="font-weight: 700;">Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($otherLayers as $layer)
                                                        <tr>
                                                            <td>{{($loop->index) + 1 }}</td>
                                                            <td>{{$layer->table_name}}</td>
                                                            <td>
                                                                <select onchange="changeDynamicallSelect({{$layer}})" class="form-control" name="{{$layer->table_name}}_cat" id="{{$layer->table_name}}_cat">
                                                                    <option value="%">Select Category</option>
                                                                    @foreach($layerCategories as $category)
                                                                        <option data-Id="{{$category->id}}" value="{{$category->name}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="form-control" name="{{$layer->table_name}}_subcat" id="{{$layer->table_name}}_subcat">
                                                                    <option value="%" disabled>Select Sub Category</option>
                                                                </select>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <span>
                                                                    <input
                                                                        type="checkbox"
                                                                        value="{{$layer->id}}"
                                                                        class="otherCheckbox btn"
                                                                        id="{{$layer->table_name}}"
                                                                        name="layersCheckbox" {{$layer->display == true ? 'checked' : ''}} />
{{--                                                                    {{ $layer->name }}--}}
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <button onclick="btnDisable({{$layer}})" class="btn btn-success">Change Category</button>
                                                                {{--@if($layer->display == true)--}}
                                                                    {{--<button onclick="btnDisable({{$layer}})" class="btn btn-success">Disable</button>--}}
                                                                {{--@else--}}
                                                                    {{--<button onclick="btnEnable({{$layer}})" class="btn btn-success">Enable</button>--}}
                                                                {{--@endif--}}
                                                                <button onclick="btnDelete({{$layer}})" class="btn btn-danger">Delete</button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {{--@foreach($otherLayers as $layer)--}}
                                                    {{--<div id="otherLayers" style="border-bottom: 1px solid gray;">--}}
                                                        {{--<div class="row">--}}
                                                            {{--<div class="col-sm-4">--}}
                                                                {{--<input--}}
                                                                    {{--type="checkbox"--}}
                                                                    {{--value="{{$layer->id}}"--}}
                                                                    {{--class="otherCheckbox"--}}
                                                                    {{--id="{{$layer->table_name}}"--}}
                                                                    {{--name="layersCheckbox" {{$layer->display == true ? 'checked' : ''}} /> {{ $layer->name }}--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-sm-4">--}}
                                                                {{--<select class="form-control" name="cat" id="cat">--}}
                                                                    {{--@foreach($layerCategories as $category)--}}
                                                                        {{--<option value="{{$category->name}}">{{$category->name}}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-sm-4">--}}
                                                                {{--<select class="form-control" name="cat" id="cat">--}}
                                                                    {{--@foreach($layerCategories as $category)--}}
                                                                        {{--<option value="{{$category->name}}">{{$category->name}}</option>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</select>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--@endforeach--}}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="styleSelector"></div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('api/layer_category')}}",
                method: 'GET',
                success: (data) => {
                    for (var i=0; i<data.length; i++){
                        var row = "<tr><td>"+data[i].name+"</td><td><a href='#'>Delete</a></td></tr>";
                        $('#pushLayerCategory').append(row);
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            });

            {{--$("#cat").change(function(){--}}
                {{--var category_id = $('option:selected', this).attr('data-Id');--}}
                {{--var formData = new FormData();--}}
                {{--formData.append('category_id', category_id);--}}
                {{--$.ajax({--}}
                    {{--type:'POST',--}}
                    {{--url: "{{ url('api/get_by_category_id')}}",--}}
                    {{--data: formData,--}}
                    {{--cache:false,--}}
                    {{--contentType: false,--}}
                    {{--processData: false,--}}
                    {{--success: (data) => {--}}
                        {{--console.log(data);--}}
                        {{--$("#subcat").empty();--}}
                        {{--$("#subcat").append("<option value='%' disabled>Select Sub Category</option>");--}}
                        {{--for(var i=0; i<data.length; i++) {--}}
                            {{--var ht = "<option value='"+data[i].name+"' >"+data[i].name+"</option>";--}}
                            {{--$("#subcat").append(ht);--}}
                        {{--}--}}
                    {{--},--}}
                    {{--error: function(data){--}}
                        {{--console.log(data);--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}

            $(".otherCheckbox").change(function () {
                console.log($(this).attr('id'));
                console.log($(this).val());
                var formData = new FormData();
                formData.append('id', $(this).val());
                formData.append('checked', $(this).prop('checked'));
                $.ajax({
                    type:'POST',
                    url: "{{ url('layer-check-enable')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
            $(".adminCheckbox").change(function () {
                console.log($(this).attr('id'));
                console.log($(this).val());
                var formData = new FormData();
                formData.append('id', $(this).val());
                formData.append('checked', $(this).prop('checked'));
                $.ajax({
                    type:'POST',
                    url: "{{ url('adminlayer-check')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        console.log(data);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });

            // Add Category Ajax function
            $('#laravel-ajax-category').submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url: "{{ url('api/layer_category')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        this.reset();
                        alert('Category has been added successfully');
                        console.log(data);
                        var row = "<tr><td>"+data.name+"</td><td><a href='#'>Delete</a></td></tr>";
                        $('#pushLayerCategory').append(row);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });

        function changeDynamicallSelect(obj) {
            var category_id = $('option:selected', $("#"+obj.table_name+"_cat")).attr('data-Id');
            var formData = new FormData();
            formData.append('category_id', category_id);
            $.ajax({
                type:'POST',
                url: "{{ url('api/get_by_category_id')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    $("#"+obj.table_name+"_subcat").empty();
                    $("#"+obj.table_name+"_subcat").append("<option value='%' disabled>Select Sub Category</option>");
                    for(var i=0; i<data.length; i++) {
                        var ht = "<option value='"+data[i].name+"' >"+data[i].name+"</option>";
                        $("#"+obj.table_name+"_subcat").append(ht);
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
        function btnEnable(obj) {
            console.log(obj);
            var category_name = $("#"+obj.table_name+"_cat").val();
            var sub_category_name = $("#"+obj.table_name+"_subcat").val();
            var formData = new FormData();
            formData.append('id', obj.id);
            formData.append('checked', true);
            formData.append('category_name', category_name);
            formData.append('sub_category_name', sub_category_name);
            $.ajax({
                type:'POST',
                url: "{{ url('layer-check')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
        function btnDelete(obj) {
            console.log(obj);
        }
        function btnDisable(obj) {
            console.log(obj);
            var category_name = $("#"+obj.table_name+"_cat").val();
            var sub_category_name = $("#"+obj.table_name+"_subcat").val();
            var formData = new FormData();
            formData.append('id', obj.id);
            formData.append('checked', false);
            formData.append('category_name', category_name);
            formData.append('sub_category_name', sub_category_name);
            $.ajax({
                type:'POST',
                url: "{{ url('layer-check')}}",
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
    </script>
@endpush
