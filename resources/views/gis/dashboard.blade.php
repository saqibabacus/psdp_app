@extends('layout-new')

@section('content')
    <div class="row" style="height:100%">
        <div class="col l2" style="border: 1px solid red;height:100%;">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">CSV Import</div>

                        <div class="panel-body">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col l10" style="border: 1px solid red;height:100%;" id="map">

        </div>
    </div>
@endsection

@push('scripts')
    <script  src="{{asset('functions/admin.js')}}"></script>
    <!-- <script>
        $(document).ready(function () {
            $( "#draggable" ).draggable();
            $( "#accordion" ).accordion();
            $("#gis_btn_close_buffer").click(function () {
                $("#draggable").css("display", "none");
            });
            $( "#sortable" ).sortable();
            $( "#sortable" ).disableSelection();
        });
    </script> -->
@endpush
