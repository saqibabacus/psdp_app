@extends('layouts.layout')

@section('content')

    @include('layouts.nav')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            @include('layouts.sidebar', ['type' => 'manage-categories'])
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row mt-2">
                                    <div class="offset-1 col-sm-5">
                                        <div class="card h-100">
                                            <div class="card-header">
                                                Add Categories
                                            </div>
                                            <div class="card-block">
                                                <form method="POST" enctype="multipart/form-data" id="laravel-ajax-category" action="javascript:void(0)" >
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-primary" name="name" placeholder="Please Add Category" id="name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button type="submit" class="btn btn-primary">Add Category</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="card h-100">
                                            <div class="card-header">
                                                Sub Categories
                                            </div>
                                            <div class="card-block">
                                                <form method="POST" enctype="multipart/form-data" id="laravel-ajax-sub-category" action="javascript:void(0)" >
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <select class="form-control" name="layer_category_id" id="layer_category_id">
                                                                    @foreach($layerCategories as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control form-control-primary" name="name" placeholder="Please Add Category" id="name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <button type="submit" class="btn btn-primary">Add Category</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-sm-3">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Layers Categories</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="row">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th style="font-weight: 700;">Category Name</th>
                                                            <th style="font-weight: 700;">Operation</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="pushLayerCategory">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Layers Sub Categories</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="row">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th style="font-weight: 700;">Sub Category Name</th>
                                                            <th style="font-weight: 700;">Category</th>
                                                            <th style="font-weight: 700;">Delete</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="pushLayerSubCategory">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="styleSelector"></div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ url('api/layer_category')}}",
                method: 'GET',
                success: (data) => {
                    for (var i=0; i<data.length; i++){
                        var row = "<tr><td>"+data[i].name+"</td><td><button onclick='deleteCategory(this, "+data[i].id+")' class='btn btn-link'>Delete</button></td></tr>";
                        $('#pushLayerCategory').append(row);
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            });

            $.ajax({
                url: "{{ url('api/layer_sub_category')}}",
                method: 'GET',
                success: (data) => {
                    console.log(data);
                    for (var i=0; i<data.length; i++){
                        var row = "<tr><td>"+data[i].name+"</td><td>"+data[i].cat_name+"</td><td><button data='"+data[i].id+"' onclick='deleteSubCategory(this, "+data[i].id+")'  id='sub-category-delete' class='btn btn-link'>Delete</button></td></tr>";
                        $('#pushLayerSubCategory').append(row);
                    }
                },
                error: (error) => {
                    console.log(error);
                }
            });

            // Add Category Ajax function
            $('#laravel-ajax-category').submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url: "{{ url('api/layer_category')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        this.reset();
                        alert('Category has been added successfully');
                        console.log(data);
                        var row = "<tr><td>"+data.name+"</td><td><button onclick='deleteCategory(this, "+data.id+")' class='btn btn-link'>Delete</button></td></tr>";
                        $('#pushLayerCategory').append(row);
                        $("#layer_category_id").append("<option value='"+data.id+"'>"+data.name+"</option>");
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });

            $('#laravel-ajax-sub-category').submit(function(e) {
                e.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type:'POST',
                    url: "{{ url('api/layer_sub_category')}}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        this.reset();
                        alert('Sub Category has been added successfully');
                        console.log(data);
                        var row = "<tr><td>"+data.name+"</td><td>"+data.cat_name+"</td><td><button data='"+data.id+"' onclick='deleteSubCategory(this, "+data.id+")' id='sub-category-delete' class='btn btn-link'>Delete</button></td></tr>";
                        $('#pushLayerSubCategory').append(row);
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
        });

        function deleteCategory(e, id) {
            $.ajax({
                type:'delete',
                url: "{{ url('api/layer_category')}}/" + id,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    $(e).parents('tr').remove();
                },
                error: function(data){
                    console.log(data);
                }
            });
        }

        function deleteSubCategory(e, id) {
            $.ajax({
                type:'delete',
                url: "{{ url('api/layer_sub_category')}}/" + id,
                cache:false,
                contentType: false,
                processData: false,
                success: (data) => {
                    console.log(data);
                    $(e).parents('tr').remove();
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
    </script>
@endpush
