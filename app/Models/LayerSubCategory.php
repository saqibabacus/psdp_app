<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayerSubCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'layer_category_id'
    ];

    public function category()
    {
        return $this->belongsTo(LayerCategory::class, 'layer_category_id');
    }
}
