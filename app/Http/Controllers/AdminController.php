<?php

namespace App\Http\Controllers;


use App\Models\LayerCategory;
use App\Models\Layers;
use App\Models\OtherLayers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;

class AdminController extends Controller
{
    public function index()
    {
        $layers = Layers::all();
        $OtherLayers = OtherLayers::all();
        $layerCategories = LayerCategory::all();
        return view('gis.dashboard2')->with(['layers' => $layers, 'otherLayers' => $OtherLayers, 'layerCategories' => $layerCategories]);
    }

    public function addLayers()
    {
        return view('gis.add-layer');
    }

    public function getAllDisplayLayers() {
        $info = DB::table('other_layers')
            ->select(DB::raw("name,table_name,category_name,sub_category_name"))
            ->where("display", "=", true)
            ->orderBy('category_name', 'asc')
            ->orderBy('sub_category_name', 'asc')
            ->get();
//        $info = DB::raw("select name,table_name,category_name,sub_category_name from other_layers where display = true order by category_name,sub_category_name;");
        return $info;
    }

    public function manageCategories()
    {
        $layerCategories = LayerCategory::all();
        return view('gis.manage-categories')->with(['layerCategories' => $layerCategories]);
    }

    public function importCsv(Request $request)
    {
        // $cols = ["id","code","amount","user_id","user_id","user_id","user_id","user_id","user_id","user_id"];
        // $path = $request->file('csv_file')->getRealPath();
        // $csv = file($path);
        // foreach ($csv as $line_index => $line) {
        //     if ($line_index > 0) { // I assume the the first line contains the column names.
        //         $newLine = [];
        //         $values = explode(',', $line);
        //         foreach ($values as $col_index => $value) {
        //             $newLine[$cols[0]] = $value;
        //         }
        //         $output[] = $newLine;
        //     }
        // }
        // $json_output = json_encode($output);
        // return $json_output;
        // $csv= file_get_contents($request->file('csv_file')->getRealPath());
        // $array = array_map("str_getcsv", explode("\n", $csv));
        // $json = json_encode($array);
        // dd(json_encode($csv));
//        return $request->file('csv_file')->getRealPath();
        try {
            // Open Shapefile
            $Shapefile = new ShapefileReader('storage/hong_kong_location.shp');

            // Read all the records
            while ($Geometry = $Shapefile->fetchRecord()) {
                // Skip the record if marked as "deleted"
                if ($Geometry->isDeleted()) {
                    continue;
                }

                // Print Geometry as an Array
//                print_r($Geometry->getArray());

                // Print Geometry as WKT
//                print_r($Geometry->getWKT());

                // Print Geometry as GeoJSON
                print_r($Geometry->getGeoJSON());

                // Print DBF data
//                print_r($Geometry->getDataArray());
            }

        } catch (ShapefileException $e) {
            // Print detailed error information
            echo "Error Type: " . $e->getErrorType()
                . "\nMessage: " . $e->getMessage()
                . "\nDetails: " . $e->getDetails();
        }

    }

}
