<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $districts = DB::table('tbl_districts')
            ->select('gid', 'adm2_en as name')
            ->where('adm1_en', '=', 'Balochistan')
            ->orderBy('adm2_en', 'asc')
            ->get();
        return response([ 'districts' => $districts, 'message' => 'Districts Retrieved successfully'], 200);
    }

    public function tehsils(Request $request)
    {
        $dist = $request->dist;
        if($dist == '%') {
            $tehsils = DB::table('tbl_tehsils')
                ->select('gid', 'adm3_en as name')
                ->where('adm1_en', '=', 'Balochistan')
                ->orderBy('adm3_en', 'asc')
                ->get();
        } else {
            $tehsils = DB::table('tbl_tehsils')
                ->select('gid', 'adm3_en as name')
                ->where('adm1_en', '=', 'Balochistan')
                ->where('adm2_en', '=', "$dist")
                ->orderBy('adm3_en', 'asc')
                ->get();
        }

        return response([ 'tehsils' => $tehsils, 'message' => 'Tehsils Retrieved successfully'], 200);
    }

    public function districts_geojson() {
        $districts = DB::select("select * from districts_geojson;");
        return response([ 'districts_geojson' => $districts, 'message' => 'Districts Retrieved successfully'], 200);
    }

    public function tehsils_geojson() {
        $districts = DB::select("select * from tehsils_geojson;");
        return response([ 'tehsils_geojson' => $districts, 'message' => 'Tehsils Retrieved successfully'], 200);
    }

    public function settlements_geojson() {

        $settlements = DB::select("select * from settlements_geojson;");
        return response([ 'settlements_geojson' => $settlements, 'message' => 'Settlements Retrieved successfully'], 200);
    }

    public function raw_geojson(Request $request) {
        $settlements = DB::select("SELECT json_build_object('features', json_agg(json_build_object('type','Feature','geometry',   ST_AsGeoJSON(geom)::json,'properties', json_build_object('attr', dbfdata::json))))FROM $request->table_name limit 500;");
        $tableName = DB::select("select * from other_layers where table_name = '$request->table_name';");
        return response([ 'settlements_geojson' => $settlements, 'table' => $tableName, 'message' => 'Settlements Retrieved successfully'], 200);
    }

    public function raw_geojson_district_tehsil(Request $request) {
        if($request->tid == null) {
            $settlements = DB::select("select json_build_object('features', json_agg(json_build_object('type','Feature','geometry',   ST_AsGeoJSON(inf.geom)::json,'properties', json_build_object('attr', inf.dbfdata::json)))) from tbl_districts d join $request->table_name inf on st_intersects(st_setsrid(ST_GeomFromText(inf.geom), 4326), d.geom) where d.gid = $request->did;");
        } elseif($request->did == null) {
            $settlements = DB::select("select json_build_object('features', json_agg(json_build_object('type','Feature','geometry',   ST_AsGeoJSON(inf.geom)::json,'properties', json_build_object('attr', inf.dbfdata::json)))) from tbl_tehsils d join $request->table_name inf on st_intersects(st_setsrid(ST_GeomFromText(inf.geom), 4326), d.geom) where d.gid = $request->tid;");
        }
        $tableName = DB::select("select * from other_layers where table_name = '$request->table_name';");
        return response([ 'settlements_geojson' => $settlements, 'table' => $tableName, 'message' => 'Settlements Retrieved successfully'], 200);
    }

    public function total_on_layer() {
        $otherlayers = DB::select("select * from other_layers where display = true;");
        return response([ 'otherlayers' => $otherlayers], 200);
    }

    public function get_summary() {
        $tables = DB::select("select table_name from other_layers where display = true;");
        $data = [];
        foreach ($tables as $tbl) {
            $str = explode("_", $tbl->table_name);
            array_shift($str);
            $str = implode("_", $str);
            $d = [
                'name' => $str,
                'summary' => DB::select("select * from get_summary(null::".$tbl->table_name.");")
            ];
            array_push($data, $d);
        }
        return $data;
    }

    public function get_summary_single(Request $request) {
        $tables = DB::select("select table_name from other_layers where display = true;");
        $data = [];
        foreach ($tables as $tbl) {
            $str = explode("_", $tbl->table_name);
            array_shift($str);
            $str = implode("_", $str);
//            return $tbl->table_name;
            $rst = DB::select("select * from get_summary_single(null::".$tbl->table_name.", '$request->_prv', '$request->_typ');");
            $d = [
                'layer_name' => $str,
                'name' => $rst[0]->name,
                'total' => $rst[0]->total
            ];
            array_push($data, $d);
        }
        return $data;
    }
}
