<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
//use Validator,Redirect,Response,File;
use Illuminate\Http\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;
use ZanySoft\Zip\Zip;
use ZipArchive;
use Illuminate\Filesystem\Filesystem;


class FileController extends Controller
{
    public function store(Request $request){
        if ($files = $request->file('shapeFileBorwser')) {

            $path = Storage::putFile('public/tempdata', new File($request->file('shapeFileBorwser')));
            $url = Storage::url($path);
            $zip = new ZipArchive();
            $res = $zip->open(public_path('storage/tempdata/'. basename($url)));
            if ($res === TRUE) {
                $zip->extractTo('storage/tempdata/newzipfolder');
                $zip->close();
            } else {
                return 'failed, code:' . $res;
            }

            $allFilesInFolder = Storage::disk('public')->allFiles('tempdata/newzipfolder');
            $fname = "";
            $shapefilecheck = ['dbf', 'prj', 'shp', 'shx'];
            $shapefilecontaintingFolder = [];
            foreach ($allFilesInFolder as $all) {
                if(in_array(explode('.', $all)[1], $shapefilecheck)){
                    array_push($shapefilecontaintingFolder, explode('.', $all)[1]);
                    if(explode('.', $all)[1] == 'shp') {
                        $fname = explode('/', $all)[2];
                    }
                }
            }
            if(count($shapefilecontaintingFolder) > 2) {
                try {
                    $Shapefile = new ShapefileReader(public_path('storage/tempdata/newzipfolder/'. $fname));
                    $tblName = "browse_".str_replace(' ', '', explode('.', $fname)[0]);
                    DB::statement("drop table if exists $tblName;");
                    DB::statement("create table $tblName (id serial, dbfdata text, geom text)");
                    while ($Geometry = $Shapefile->fetchRecord()) {
                        // Skip the record if marked as "deleted"
                        if ($Geometry->isDeleted()) {
                            continue;
                        }
                        $g = "'".$Geometry->getWKT()."'";
                        $d = "'".json_encode($Geometry->getDataArray())."'";
//                        dd($Geometry->getDataArray());

                        DB::statement("insert into $tblName (dbfdata, geom) values ($d, $g)");

//                        print_r($Geometry->getGeoJSON());
                    }
                    $nn = explode('.', $fname)[0];
//                    $checkTable = DB::statement("select * from other_layers where name = '$nn'");
                    $checkTable = DB::select(DB::raw("select name from other_layers where name = '".$nn."'"));

                    if(count($checkTable) < 1) {
                        DB::statement("INSERT INTO other_layers (name, table_name, display) values ('$nn', '$tblName', true)");
                    }
                    $deltefile = new Filesystem;
                    $deltefile->cleanDirectory(public_path('storage/tempdata/newzipfolder'));

                } catch (ShapefileException $e) {
                    return $e->getMessage();
                }
            }

            return Response()->json([
                "success" => true,
                "file" => "File Added."
            ]);

        }

        return Response()->json([
            "success" => false,
            "file" => ''
        ]);
    }

    function createZip($zip,$dir){
        if (is_dir($dir)){

            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){

                    // If file
                    if (is_file($dir.$file)) {
                        if($file != '' && $file != '.' && $file != '..'){

                            $zip->addFile($dir.$file);
                        }
                    }else{
                        // If directory
                        if(is_dir($dir.$file) ){

                            if($file != '' && $file != '.' && $file != '..'){

                                // Add empty directory
                                $zip->addEmptyDir($dir.$file);

                                $folder = $dir.$file.'/';

                                // Read data of the folder
                                createZip($zip,$folder);
                            }
                        }

                    }

                }
                closedir($dh);
            }
        }
    }

    public function layersCheck(Request $request) {
        $result = DB::statement("update public.other_layers set display = $request->checked, category_name = '$request->category_name', sub_category_name = '$request->sub_category_name' where id = $request->id;");
        return $result;
    }

    public function layersCheckEnable(Request $request) {
        $result = DB::statement("update public.other_layers set display = $request->checked where id = $request->id;");
        return $result;
    }

    public function adminLayersCheck(Request $request) {
        $result = DB::statement("update public.layers set display = $request->checked where id = $request->id;");
        return $result;
    }
}
