<?php

namespace App\Http\Controllers;

use App\Models\LayerCategory;
use App\Models\OtherLayers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LayerCategoryController extends Controller
{
    public function index() {
        $layerCategories = LayerCategory::all();
        return $layerCategories;
    }

    public function store(Request $request) {
        try {
            $info = LayerCategory::firstOrCreate($request->all());
            return $info;
        } catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function show($id) {
        try {
            $info = LayerCategory::findOrFail($id);
            return $info;
        }catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function update(Request $request, $id) {
        try {
            $info = LayerCategory::where('id', $id)->update($request->all());
            if($info) {
                return ['msg' => 'Model Updated Successfully'];
            }
        } catch (QueryException $e) {
            $e->getMessage();
        }
    }

    public function destroy($id) {
        try {
            $info = LayerCategory::destroy($id);
            if($info) {
                return ['msg' => 'Model Deleted'];
            }
        } catch (ModelNotFoundException $e) {
            $e->getMessage();
        }
    }

    public function getOtherLayers() {
        $sampleData = [];
        $allOtherLayers = OtherLayers::where('display', '=', true)->get();
        foreach ($allOtherLayers as $row) {
            if(count($sampleData) > 0) {
                foreach ($sampleData as $key=>$d) {
                    if($d["category_name"] == $row->category_name) {
                        $b = [
                            "sub_category_name" => $row->sub_category_name,
                            "name" => $row->name,
                            "table_name" => $row->table_name
                        ];
                        array_push($sampleData[$key]["sub_category_name"], $b);
                    } else {
                        $a = [
                            "category_name" => $row->category_name,
                            "sub_category_name" => [
                                [
                                    "sub_category_name" => $row->sub_category_name,
                                    "name" => $row->name,
                                    "table_name" => $row->table_name
                                ]
                            ]
                        ];
                        array_push($sampleData, $a);
                    }
                }
            } else {
                $a = [
                    "category_name" => $row->category_name,
                    "sub_category_name" => [
                        [
                            "sub_category_name" => $row->sub_category_name,
                            "name" => $row->name,
                            "table_name" => $row->table_name
                        ]
                    ]
                ];
                array_push($sampleData, $a);
            }
//            if(!in_array($row->category_name, $sampleData)) {

//            }
        }
        return $sampleData;
    }
}
