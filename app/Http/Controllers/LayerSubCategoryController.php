<?php

namespace App\Http\Controllers;

use App\Models\LayerCategory;
use Illuminate\Http\Request;
use App\Models\LayerSubCategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class LayerSubCategoryController extends Controller
{
    public function index() {
        $layerSubCategories = [];
        $layerCategories = LayerSubCategory::all();
        foreach ($layerCategories as $layerCategory) {
            $subOne = [
                "id" => $layerCategory->id,
                "name"=> $layerCategory->name,
                "cat_name" => $layerCategory->category->name,
            ];
            array_push($layerSubCategories, $subOne);
        }
        return $layerSubCategories;
    }

    public function store(Request $request) {
        try {
            $info = LayerSubCategory::firstOrCreate($request->all());
            $returnInfo = [
                "id" => $info->id,
                "name"=> $info->name,
                "cat_name" => $info->category->name,
            ];
            return $returnInfo;
        } catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function show($id) {
        try {
            $info = LayerSubCategory::findOrFail($id);
            return $info;
        }catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }

    public function update(Request $request, $id) {
        try {
            $info = LayerSubCategory::where('id', $id)->update($request->all());
            if($info) {
                return ['msg' => 'Model Updated Successfully'];
            }
        } catch (QueryException $e) {
            $e->getMessage();
        }
    }

    public function destroy($id) {
        try {
            $info = LayerSubCategory::destroy($id);
            if($info) {
                return ['msg' => 'Model Deleted'];
            }
        } catch (ModelNotFoundException $e) {
            $e->getMessage();
        }
    }

    public function getByCategoryId(Request $request) {
        try {
            $info = LayerSubCategory::where('layer_category_id', '=', $request->category_id)->get();
            return $info;
        } catch (ModelNotFoundException $e) {
            return $e->getMessage();
        }
    }
}
