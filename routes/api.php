<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\LayerCategoryController;
use App\Http\Controllers\LayerSubCategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::apiResource('districts', AdminController::class);
Route::get('/districts', [AdminController::class, 'index']);
Route::get('/tehsils', [AdminController::class, 'tehsils']);
Route::get('/districts_geojson', [AdminController::class, 'districts_geojson']);
Route::get('/tehsils_geojson', [AdminController::class, 'tehsils_geojson']);
Route::get('/settlements_geojson', [AdminController::class, 'settlements_geojson']);
Route::post('/raw_geojson', [AdminController::class, 'raw_geojson']);
Route::post('/raw_geojson_district_tehsil', [AdminController::class, 'raw_geojson_district_tehsil']);
Route::get('/total_on_layer', [AdminController::class, 'total_on_layer']);

Route::get('/layer_category', [LayerCategoryController::class, 'index']);
Route::post('/layer_category', [LayerCategoryController::class, 'store']);
Route::delete('/layer_category/{id}', [LayerCategoryController::class, 'destroy']);
Route::get('/get_other_layers', [LayerCategoryController::class, 'getOtherLayers']);

Route::get('/layer_sub_category', [LayerSubCategoryController::class, 'index']);
Route::post('/layer_sub_category', [LayerSubCategoryController::class, 'store']);
Route::delete('/layer_sub_category/{id}', [LayerSubCategoryController::class, 'destroy']);
Route::post('/get_by_category_id', [LayerSubCategoryController::class, 'getByCategoryId']);

Route::get('/get_summary', [AdminController::class, 'get_summary']);
Route::post('/get_summary_single', [AdminController::class, 'get_summary_single']);
