<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FileController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
//    if($request->all()) {
        return view('gis.main')->with('d', $request->all());
//    } else {
//        return view('gis.main');
//    }
});

//Route::get('/gis/home', function () {
//    return view('gis.home');
//});

Route::get('/gis/main', function () {
    return view('gis.main');
});
Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::get('/add-layers', [AdminController::class, 'addLayers'])->name('add-layers');
    Route::get('/get-all-layers', [AdminController::class, 'getAllDisplayLayers'])->name('get-all-layers');
    Route::get('/manage-categories', [AdminController::class, 'manageCategories'])->name('manage-categories');
    Route::post('/store-file', [FileController::class, 'store'])->name('store-file');
    Route::post('/layer-check', [FileController::class, 'layersCheck'])->name('layer-check');
    Route::post('/layer-check-enable', [FileController::class, 'layersCheckEnable'])->name('layer-check-enable');
    Route::post('/adminlayer-check', [FileController::class, 'adminLayersCheck'])->name('adminlayer-check');
    Route::post('/import_parse', [AdminController::class, 'importCsv'])->name('import_parse');
});
