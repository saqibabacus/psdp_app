var geoserverWMSUrl = 'http://116.0.52.20:8080/geoserver/AdminBoundaries/wms';
var geoserverOwsUrl = 'http://116.0.52.20:8080/geoserver/ows?';
var map,highlighFeature=null;
var mainUrl = 'http://localhost:8000/api/';
var districtsLayerGeoJson, tehsilsLayerGeoJson, settlementsLayerGeoJson, bufferedPoint,bufferGeojson, marker, anonymousLayer, anonymousLayerGroup,schemeMarkersGroup,filterAnonymousLayer = null;
var selectedDistrictLayer = null;
var distance = 0;
var layerGroupLayers = [];
var schemeUrl = "http://3.127.198.171:50505/api/gisdata/GetSchemeLocation";
$(document).ready(function(){
    var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

    var grayscale = L.tileLayer(mbUrl, { id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr }),
        streets = L.tileLayer(mbUrl, { id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr }),
        googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
            maxZoom: 20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        }),
        googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
            maxZoom: 20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        }),
        googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
            maxZoom: 20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        }),
        googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
            maxZoom: 20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
        });

    var districstLayer = L.tileLayer.wms(geoserverWMSUrl, {
        layers: 'AdminBoundaries:tbl_districts',
        format: 'image/png',
        transparent: true,
        tiled: true
    });

    map = L.map('map', {
        center: [29.083270, 66.996452],
        zoom: 7,
        layers: [googleStreets]
    });

    var baseLayers = {
        "Google Street": googleStreets,
        "Google Hybrid": googleHybrid,
        // "Google Satellite": googleSat,
        // "Google Terrain": googleTerrain
    };


    // var adminBoundaries = L.layerGroup(districstLayer).addTo(map);
    anonymousLayerGroup = L.layerGroup();
    schemeMarkersGroup = L.layerGroup();

    var overlays = {
        "Schemes": schemeMarkersGroup
    };

    var layerControl = L.control.layers(baseLayers, overlays, { collapsed: false }).addTo(map);

    // showDistrict('Lahore');
    // getAllDistricts();
    getAllDistrictsAndFill();
    getAllTehsilsAndFill('%');
    getDistrictsGeojson()
        .then(function (data) {
            // console.log(JSON.parse(data[0].json_build_object).features);
            districtsLayerGeoJson = L.geoJSON(JSON.parse(data[0].json_build_object).features, {

                style: style,

                onEachFeature: onEachFeature,

                pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, {
                        radius: 8,
                        fillColor: "#ff7800",
                        color: "#000",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 0.8
                    });
                }
            }).addTo(map);
            // layerControl.addOverlay(districtsLayerGeoJson, "Districts");
        })
        .catch(function (error) {
            console.log(error)
        });
    getTehsilsGeojson()
        .then(function (data) {
            tehsilsLayerGeoJson = L.geoJSON(JSON.parse(data[0].json_build_object).features, {

                style: {
                    fillColor: "#228B22",
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '0',
                    fillOpacity: 0.7
                },

                onEachFeature: onEachFeature,

                pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, {
                        radius: 8,
                        fillColor: "#228B22",
                        color: "#000",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 0.8
                    });
                }
            });
            // layerControl.addOverlay(tehsilsLayerGeoJson, "Tehsils");
        })
        .catch(function (error) {
            console.log(error);
        });
    displayAllSchemes()
        .then(function (data) {
            var schemeMarkers = data.response;
            schemeMarkers.forEach(function(scheme){
                var coords = scheme.coordinates;
                if(coords != null) {
                    if(coords.split(" ").length == 2) {
                        var lng = coords.split(" ")[0];
                        var lat = coords.split(" ")[1];
                        var icn = L.icon({
                            iconUrl: '../images/marker-red.png',
                            iconSize: [38, 38],
                        });
                        var marker = L.marker([lat, lng],{
                            icon: icn
                        }).bindPopup(scheme.schemeName);
                        schemeMarkersGroup.addLayer(marker);
                    }
                }
            });
        })
        .catch(function (err) {
            console.log(err);
        });
    // total_on_layers();
    $.ajax({
        url: mainUrl + 'total_on_layer' ,
        dataType: 'JSON',
        method: 'GET',
        success: function (data) {
            $.each(data, function (index, value) {
                for(var i=0; i<value.length; i++) {
                    raw_table(value[i]['table_name'])
                        .then(function(data){
                            anonymousLayer = L.geoJSON(JSON.parse(data.settlements_geojson[0].json_build_object).features, {
                                id: data.table[0].table_name,
                                pointToLayer: function (feature, latlng) {
                                    return L.circleMarker(latlng, {
                                        radius: 8,
                                        fillColor: "#ff7800",
                                        color: "#000",
                                        weight: 1,
                                        opacity: 1,
                                        fillOpacity: 0.8
                                    });
                                }
                            });
                            // layerControl.addOverlay(anonymousLayer, data.table[0].name);
                            anonymousLayerGroup.addLayer(anonymousLayer);
                        }).catch(function (error) {
                        console.log(error);
                    });
                }
            })
        },
        error: function (error) {
            console.log(error);
        }
    });

    $('#gis_districts').change(function (e) {
        var gid = $("#gis_districts option:selected").val();
        var text = $("#gis_districts option:selected").text();
        getAllTehsilsAndFill(text);
        districtsLayerGeoJson.eachLayer(function (layer) {
            if(layer.feature.properties.gid == gid) {
                if(selectedDistrictLayer != null) {
                    map.removeLayer(selectedDistrictLayer);
                }
                selectedDistrictLayer = L.geoJSON(layer.feature, {
                    style: {
                        color: 'cyan',
                        weight: 4
                    }
                }).addTo(map);
                map.fitBounds(layer.getBounds());
                var dn = layer.feature.properties.name;
                displaySUmmary(dn.toUpperCase(), 'DISTRICT');
            }
        });
        // filteringAssets
        if(filterAnonymousLayer != null) {
            map.removeLayer(filterAnonymousLayer);
        }
        anonymousLayerGroup.eachLayer(function(layer){
            if(layer._mapToAdd != null || layer._mapToAdd != undefined) {
                var tableName = layer.options.id;
                raw_table_district_tehsil(tableName, gid,null)
                    .then(function(data){
                        filterAnonymousLayer = L.geoJSON(JSON.parse(data.settlements_geojson[0].json_build_object).features, {
                            id: data.table[0].table_name,
                            pointToLayer: function (feature, latlng) {
                                console.log(feature);
                                return L.circleMarker(latlng, {
                                    radius: 8,
                                    fillColor: "blue",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                }).bindPopup(feature.properties.attr.NAME);
                            }
                        });
                        map.addLayer(filterAnonymousLayer);
                    }).catch(function (error) {
                    console.log(error);
                });
            }
        });
    });
    $('#gis_tehsils').change(function (e) {
        if(!map.hasLayer(tehsilsLayerGeoJson)) {
            map.addLayer(tehsilsLayerGeoJson);
        }
        if(map.hasLayer(districtsLayerGeoJson)) {
            map.removeLayer(districtsLayerGeoJson);
        }
        var gid = $("#gis_tehsils option:selected").val();
        tehsilsLayerGeoJson.eachLayer(function (layer) {
            if(layer.feature.properties.gid == gid) {
                layer.setStyle({
                    color: 'cyan',
                    weight: 4
                });
                map.fitBounds(layer.getBounds());
                var dn = layer.feature.properties.name;
                displaySUmmary(dn.toUpperCase(), 'TEHSIL');
            }
        });
    });
    $('#gis_buffer').change(function (e) {

    });

    $("#gis_droppedpin").click(function () {
        console.log(layerControl);
        anonymousLayer.setZIndex(3);
        onMapClick(map.getCenter());
        $('#gis_droppedpin').attr('disabled', true);
        $('#gis_btn_buffer').attr('disabled', false);
    });

    $("#gis_btn_buffer").click(function () {
        $('#gis_btn_clrbuffer').attr('disabled', false);
        var bufferedLayer = [];
        var bufferSize = $("#gis_buffer option:selected").val();
        const incidences2 = districtsLayerGeoJson.toGeoJSON();
        var point = turf.point([marker.getLatLng().lng, marker.getLatLng().lat]);
        var buffered = turf.buffer(point, bufferSize, {units: 'kilometers'});
        bufferGeojson = L.geoJSON(buffered).addTo(map);
        map.fitBounds(bufferGeojson.getBounds());
        anonymousLayerGroup.eachLayer(function(layer){
            if(layer._mapToAdd != null || layer._mapToAdd != undefined) {
                var newHtmlTableStart = "<table class='table table-bordered'>";
                var newHtmlTableEnd = "</table>";
                var firstIteration = true;
                for(var key in layer._layers) {
                    var obj = layer._layers[key];
                    var lrf = obj.feature;
                    var retunBuffer = turf.booleanIntersects(buffered, lrf);
                    if(retunBuffer) {
                        // $("#draggable").css("display", "block");
                        var layerNameBuff = layer.options.id;
                        lrf.layerNameBuff = layerNameBuff.split("browse_")[1];
                        bufferedLayer.push(lrf);
                        if(lrf.geometry.type == 'Point') {
                            var p = lrf.geometry.coordinates;
                            distance = ((turf.distance(point, turf.point(p), "miles"))*1.60934).toFixed(2);
                        } else {
                            if((lrf.geometry.coordinates).length > 0) {
                                var arlen = parseInt(((lrf.geometry.coordinates).length)/2);
                                if(Array.isArray(lrf.geometry.coordinates[arlen][0])) {
                                    if(Array.isArray(lrf.geometry.coordinates[arlen][0][0])) {
                                        var p = lrf.geometry.coordinates[arlen][0][0];
                                        distance = ((turf.distance(point, turf.point(p), "miles"))*1.60934).toFixed(2);
                                        // var m = new L.marker(p, {
                                        //     draggable:'true',
                                        //     icon: L.icon({
                                        //         iconUrl: '../images/marker-red.png',
                                        //
                                        //         iconSize: [38, 38], // size of the icon
                                        //     })
                                        // }).addTo(map);
                                    } else {
                                        var p = lrf.geometry.coordinates[arlen][0];
                                        distance = ((turf.distance(point, turf.point(p), "miles"))*1.60934).toFixed(2);
                                        // var m = new L.marker(p, {
                                        //     draggable:'true',
                                        //     icon: L.icon({
                                        //         iconUrl: '../images/marker-red.png',
                                        //
                                        //         iconSize: [38, 38], // size of the icon
                                        //     })
                                        // }).addTo(map);
                                    }
                                } else {
                                    var p = lrf.geometry.coordinates[arlen];
                                    distance = ((turf.distance(point, turf.point(p), "miles"))*1.60934).toFixed(2);
                                    // var m = new L.marker([p[1], p[0]], {
                                    //     draggable:'true',
                                    //     icon: L.icon({
                                    //         iconUrl: '../images/marker-red.png',
                                    //
                                    //         iconSize: [38, 38], // size of the icon
                                    //     })
                                    // }).addTo(map);
                                }
                            }
                        }
                        var keys = Object.keys(lrf.properties.attr);
                        var values = Object.values(lrf.properties.attr);
                        var htmlTableStart = "<table class='table table-bordered text-center'><tbody>";
                        var htmlTableEnd = "</tbody></table>";
                        var row1start = "";
                        var row1end = "</tr>";
                        var row2start = "<tr><td>"+distance+"</td>";
                        var row2end = "</tr>";
                        for(var i=0; i<keys.length; i++) {
                            if(firstIteration == true) {
                                if(i == 0) {
                                    row1start += "<tr><th>Distance</th>";
                                }
                                row1start += "<th>"+keys[i]+"</th>";
                            }
                        }
                        for(var k=0; k<values.length; k++) {
                            row2start += "<td>"+values[k]+"</td>";
                        }
                        row1start += row1end;
                        row2start += row2end;
                        htmlTableStart += row1start;
                        htmlTableStart += row2start;
                        htmlTableStart += htmlTableEnd;
                        // --------------------------------
                        // var htmlDivStart = "<div style='border-bottom: 2px dashed gray; margin-top: 5px;'><table class='bordered'><tr><td>Distance</td><td>"+distance+"</td></tr>";
                        // var htmlDivEnd = "</table></div>";
                        // for(var i=0; i< keys.length; i++) {
                        //     htmlDivStart += "<tr><td>"+keys[i]+"</td><td>"+values[i]+"</td></tr>";
                        // }
                        // htmlDivStart += htmlDivEnd;
                        $('#bufferResultsDetails').append(htmlTableStart);
                    }
                    firstIteration = false;
                }
            } else {
                console.log("undefined or null, " + layer.options.id);
            }
        });
        bufferedPoint = L.geoJSON(bufferedLayer, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, {
                    radius: 8,
                    fillColor: "#111999",
                    color: "#000",
                    weight: 1,
                    opacity: 1,
                    fillOpacity: 0.8
                });
            }
        }).addTo(map);
        // map.addLayer(bufferedLayer);
        var countsBuffer = find_duplicate_in_array(bufferedLayer);
        var bufferHtmlTable = "<table class='table'><tr><td>Buffer Radius</td><td>"+bufferSize+"</td></table>";
        $('#bufferTable').append(bufferHtmlTable);
        var countsBufferTable = "<table class='table'><tr><th>Layer Name</th><th>Count</th></tr>";
        for(var ii=0; ii<countsBuffer.length; ii++) {
            var trow = "<tr><td>"+countsBuffer[ii].layerName+"</td><td>"+countsBuffer[ii].total+"</td></tr>";
            countsBufferTable += trow;
        }
        countsBufferTable += "</table>";
        $('#bufferTable').append(countsBufferTable);
        getReverseDetails(marker.getLatLng().lat,marker.getLatLng().lng).then(function(data){
            console.log(data);
            var DetailTable = "<table class='table'>" +
                "<tr><td>District</td><td>"+data.address.city+"</td></tr>" +
                "<tr><td>Province</td><td>"+data.address.state+"</td></tr>" +
                "<tr><td>Country</td><td>"+data.address.country+"</td></tr>" +
                "</table>";
            $('#bufferTable').append(DetailTable);
        });
    });

    $('#gis_btn_clrbuffer').click(function () {
        map.removeLayer(bufferedPoint);
        map.removeLayer(bufferGeojson);
        map.removeLayer(marker);
        // $("#draggable").css("display", "none");
    });

    map.on('click', function(e) {
        // alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
        // onMapClick(e);
    });
    map.on('zoomend', function(evt) {
        hideOverlappingTooltips();
    });
    hideOverlappingTooltips();

    $("#slide_districts").change(function () {
        var val = $('#slide_districts').val();
        districtsLayerGeoJson.setStyle({ fillOpacity: val });
    });
    $("#slide_tehsils").change(function () {
        var val = $('#slide_tehsils').val();
        tehsilsLayerGeoJson.setStyle({ fillOpacity: val });
    });
    $("#slide_settlements").change(function () {
        var val = $('#slide_settlements').val();
        settlementsLayerGeoJson.setStyle({ fillOpacity: val });
    });

    $("#checkbox_slide_districts").change(function () {
        if(this.checked) {
            map.addLayer(districtsLayerGeoJson);
        }else {
            map.removeLayer(districtsLayerGeoJson);
        }
    });
    $("#checkbox_slide_tehsils").change(function () {
        if(this.checked) {
            map.addLayer(tehsilsLayerGeoJson);
        }else {
            map.removeLayer(tehsilsLayerGeoJson);
        }
    });
    $("#checkbox_slide_settlements").change(function () {
        if(this.checked) {
            map.addLayer(settlementsLayerGeoJson);
        }else {
            map.removeLayer(settlementsLayerGeoJson);
        }
    });
});

function onMapClick(e) {
    marker = new L.marker(e, {
        draggable:'true',
        icon: L.icon({
            iconUrl: '../images/marker-pin.png',

            iconSize: [38, 38], // size of the icon
        })
    });
    marker.on('dragend', function(event){
        marker = event.target;
        var position = marker.getLatLng();
        marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
        map.panTo(new L.LatLng(position.lat, position.lng))
    });
    map.addLayer(marker);
};

function style(feature) {
    return {
        fillColor: getColor(feature.properties.Party),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '0',
        fillOpacity: 0.7
    };
}

function getColor(property) {
    if(property === 'PML-N') {
        return '#38A700'
    } else if(property === 'PTI') {
        return '#FF7E7C'
    } else if(property === 'Independent') {
        return '#E8BEFE'
    } else if(property === 'PPP') {
        return '#686867'
    } else if(property === 'PML-Q') {
        return '#D0E562'
    } else if(property === 'MQM-P') {
        return '#AAB7B8'
    } else if(property === 'MMA') {
        return '#F59AB3'
    } else if(property === 'Others') {
        return '#BA58F2'
    } else {
        return '#9B59B6'
    }
}

function onEachFeature(feature, layer) {

    // if(legendArray.map(function(e){return e.name;}).indexOf(feature.properties.Legend) == -1) {
    //     var obj = {
    //         name: feature.properties.Legend,
    //         color: getColor(feature.properties.Party)
    //     };
    //     legendArray.push(obj);
    // }
    //
    var tooltipOptions = {
        className: 'labelstyle',
        permanent: true,
        direction: 'center',
        opacity: 1.0,
        interactive: true
    }
    layer.bindTooltip(feature.properties.name, tooltipOptions).closeTooltip(); //display labels
}

function getDistrictsGeojson() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: mainUrl + 'districts_geojson' ,
            dataType: 'JSON',
            type: 'GET',
            success: function (data) {
                resolve(data.districts_geojson);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}
function getReverseDetails(lat, lon) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: "https://nominatim.openstreetmap.org/reverse?format=json&lat="+lat+"&lon="+lon+"&zoom=18&addressdetails=1",
            dataType: 'JSON',
            type: 'GET',
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}
function getTehsilsGeojson() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: mainUrl + 'tehsils_geojson' ,
            dataType: 'JSON',
            type: 'GET',
            success: function (data) {
                resolve(data.tehsils_geojson);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}
function total_on_layers() {

}
function raw_table(table) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: mainUrl + 'raw_geojson' ,
            dataType: 'JSON',
            data: {table_name: table},
            method: 'POST',
            success: function (data) {
                console.lo
                resolve(data);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

function raw_table_district_tehsil(table,did,tid) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: mainUrl + 'raw_geojson_district_tehsil' ,
            dataType: 'JSON',
            data: {table_name: table, did: did, tid:tid},
            method: 'POST',
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

function getSettlementsGeojson() {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: mainUrl + 'raw_geojson' ,
            dataType: 'JSON',
            data: {table_name: 'testTable'},
            method: 'POST',
            success: function (data) {
                console.log(data);
                resolve(data.settlements_geojson);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

function getAllDistrictsAndFill() {
    $.ajax({
        url: mainUrl + 'districts' ,
        dataType: 'JSON',
        type: 'GET',
        success: function (data) {
            var dropdown = $("#gis_districts");
            $.each(data.districts, function (index, value) {
                dropdown.append($("<option />").val(value.gid).text(value.name));
            })
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getAllTehsilsAndFill(dist) {
    $.ajax({
        url: mainUrl + 'tehsils' ,
        dataType: 'JSON',
        type: 'GET',
        data: {dist: dist},
        success: function (data) {
            console.log(data);
            var dropdown = $("#gis_tehsils");
            dropdown.empty();
            dropdown.append($("<option />").val("%").text("Select Tehsil"));
            $.each(data.tehsils, function (index, value) {
                dropdown.append($("<option />").val(value.gid).text(value.name));
            })
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getAllDistricts() {
    var url = geoserverOwsUrl +
        "service=wfs&version=1.0.0&" +
        "request=getfeature&" +
        "typename=AdminBoundaries:tbl_districts&" +
        "CQL_FILTER=1=1&" +
        "outputFormat=application/json";
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "GET",
        success: function(data)
        {
            var $dropdown = $("#districtSelect");
            for (var i=0; i<data.features.length; i++) {
                $dropdown.append($("<option />").val(data.features[i].properties.adm2_en).text(data.features[i].properties.adm2_en));
            }
        },
        error: function (err) {
            console.log(err)
        }
    });
}

function showDistrict(district) {
    var url = geoserverOwsUrl +
        "service=wfs&version=1.0.0&" +
        "request=getfeature&" +
        "typename=AdminBoundaries:tbl_districts&" +
        "CQL_FILTER=adm2_en='"+district+"'&" +
        "outputFormat=application/json";
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "GET",
        success: function(data)
        {
            console.log(data);
            if(highlighFeature != null) {
                map.removeLayer(highlighFeature);
            };
            highlighFeature = L.geoJSON(data.features[0], {
                style: function (feature) {
                    return {
                        fillColor: '',
                        weight: 5,
                        opacity: 1,
                        color: 'cyan',
                        fillOpacity: 0
                    };
                },
                onEachFeature: function (layer) {
                    // console.log(layer.geometry.getBounds());
                    // map.fitBounds(layer.getBounds());
                }
            }).addTo(map);
            map.fitBounds(highlighFeature.getBounds())
        },
        error: function (err) {
            console.log(err)
        }
    });
}

function displayAllSchemes() {
    return new Promise(function(resolve, reject){
        $.ajax({
            url: schemeUrl,
            dataType: "JSON",
            type: "GET",
            success: function(data)
            {
                resolve(data);
            },
            error: function (err) {
                reject(err);
            }
        });
    });
}

function overlap(rect1, rect2) {
    return(!(rect1.right < rect2.left ||
            rect1.left > rect2.right ||
            rect1.bottom < rect2.top ||
            rect1.top > rect2.bottom)
    );
}

function hideOverlappingTooltips() {
    var rects = [];
    var tooltips = document.getElementsByClassName('labelstyle');
    for (var i = 0; i < tooltips.length; i++) {
        tooltips[i].style.visibility = '';
        rects[i] = tooltips[i].getBoundingClientRect();
    }
    for (var i = 0; i < tooltips.length; i++) {
        if (tooltips[i].style.visibility != 'hidden') {
            for (var j = i + 1; j < tooltips.length; j++) {
                if (overlap(rects[i], rects[j])) tooltips[j].style.visibility = 'hidden';
            }
        }
    }
}
function updateOpacity(val) {
    districtsLayerGeoJson.setStyle({ fillOpacity: val });
}

function find_duplicate_in_array(array){
    let returnArray = [];
    array.forEach(item => {
        if(returnArray.length > 0) {
            var objInArray = returnArray.find(function(o){
                if(o.layerName == item.layerNameBuff) {
                    return true;
                }
            });
            if(objInArray != undefined) {
                objInArray.total = objInArray.total + 1;
            } else {
                let obj = {
                    layerName: item.layerNameBuff,
                    total: 1
                };
                returnArray.push(obj);
            }
        } else {
            let obj = {
                layerName: item.layerNameBuff,
                total: 1
            };
            returnArray.push(obj);
        }
    });
    return returnArray;
}
